from statistics import fmean, variance, stdev

list_numbers = [8.060, 9.190, 9.110, 8.100]
mean = fmean(list_numbers)

print(list_numbers)
print(f"Mean = {mean:.3f}")
print(f"Variance = {variance(list_numbers, mean):.3f}")
print(f"Standard Deviation = {stdev(list_numbers, mean):.3f}")
