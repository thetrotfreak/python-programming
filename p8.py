u = 0
v = 10
t = 20
a = (v - u)/t
print(f"Initial Velocity of the car = {u} m/s")
print(f"Final Velocity of the car = {v} m/s")
print(f"Time = {t} s")
print(f"Acceleration = {a} m/s\u00B2")
