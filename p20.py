def factorial(n):
    if n < 0:
        return None

    if n == 0:
        return 1

    return n*factorial(n-1)


m = int(input("Number? "))
print(f"Factorial of {m}, {m}! = {factorial(m)}")
