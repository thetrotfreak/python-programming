def partition(array, start, end):
    pivot = array[end]
    i = start - 1
    for j in range(start, end):
        if array[j] <= pivot:
            i = i + 1
            array[i], array[j] = array[j], array[i]

    array[i+1], array[end] = array[end], array[i+1]
    return i+1


def sort_helper(array, start, end):
    if start < end:
        pi = partition(array, start, end)
        sort_helper(array, start, pi-1)
        sort_helper(array, pi+1, end)


def quicksort(array):
    sort_helper(array, 0, len(array)-1)
    return array


nums = [50, 23, 9, 18, 61, 32]
print(quicksort(nums))
