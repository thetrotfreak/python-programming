import os


COMMENT_PREFIX = "#"
file = open("p48.backup.py", "rt+")

print("---", file.name, "(with comments, if any)", "begin", "---")
print(file.read())
file.seek(os.SEEK_SET)
print("---", file.name, "end", "---")

buff = file.readlines()

for i in range(len(buff)-1, -1, -1):
    if buff[i].lstrip().startswith(COMMENT_PREFIX):
        del buff[i]

file.seek(os.SEEK_SET)
file.truncate()
file.writelines(buff)
file.seek(os.SEEK_SET)

print("---", file.name, "(stripped of any comments)", "begin", "---")
print(file.read())
print("---", file.name, "end", "---")

file.close()
