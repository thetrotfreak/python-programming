print("Enter length of -")

a = int(input("First side of Prism? "))
b = int(input("Second side of Prism? "))
c = int(input("Third side of Prism? "))

surface_area = 2*(a*b + b*c + c*a)

print(f"Prism's Surface Area = {surface_area} square units")
