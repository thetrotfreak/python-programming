glob = "global"


def local():
    loc = "local"
    print(loc)


def access():
    print(glob)


def change():
    global glob
    glob = "global changed"
    print(glob)


local()
access()
change()
