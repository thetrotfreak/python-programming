import random

lottery = random.randint(10, 99)

drawn_lottery = int(input("Your lottery number? "))

award = 0

if lottery == drawn_lottery:
    award = 10_000
# only possible permutation of
elif lottery == ((drawn_lottery % 10)*10) + (drawn_lottery//10):
    award = 3_000
else:
    for l in list(str(lottery)):
        for dl in list(str(drawn_lottery)):
            if l == dl:
                award = 1_000
                break

print(f"Your award : {award}")
