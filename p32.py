def matrix_product(x, y):
    x_dim = len(x), len(x[0])
    y_dim = len(y), len(y[0])

    if x_dim[1] != y_dim[0]:
        return None

    z = []

    for i in range(0, x_dim[0]):
        zeroFill = []
        for j in range(0, y_dim[1]):
            zeroFill.append(0)
        z.append(zeroFill)

    for i in range(len(x)):
        for j in range(len(y[0])):
            for k in range(len(y)):
                z[i][j] += x[i][k] * y[k][j]

    return z


X = [
    [12, 7, 3],
    [4, 5, 6],
    [7, 8, 9]
]

Y = [
    [5, 8, 1, 2],
    [6, 7, 3, 0],
    [4, 5, 9, 1]
]

print(matrix_product(X, Y))
