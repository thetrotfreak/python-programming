import os


file = open("p46.txt", "rt+")

print("---", file.name, "begin", "---")
print(file.read())
print("---", file.name, "end", "---")

file.seek(os.SEEK_SET)
buff = file.readlines()

word = input("Delete a line if it has which word? ")

for line in buff:
    if word.isspace() or len(word) == 0:
        break
    if line.find(word) != -1:
        print("Deleted", "'", line, "'")
        buff.remove(line)
        break

file.seek(os.SEEK_SET)
file.truncate()

file.writelines(buff)
file.close()
