file = open("README.txt", "rt")
w = dict()

for line in file.readlines():
    words = line.split()
    for word in words:
        v = w.setdefault(word, 1)
        if v >= 1:
            w.update({word: v+1})

for word, freq in w.items():
    print(f"{word:>10} : {freq-1:<10}")

file.close()
