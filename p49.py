import os
import re


def toupper(match):
    return match.group(1) + match.group(2).upper()


file = open("README_copy.txt", "rt+")

print("---", file.name, "(before capitalization)", "begin", "---")
print(file.read())
file.seek(os.SEEK_SET)
print("---", file.name, "end", "---")

buff = file.readlines()
regex_cap_word = re.compile(r"(^|\s+)([a-z])")

for i in range(len(buff)):
    buff[i] = re.sub(regex_cap_word, toupper, buff[i])

file.seek(os.SEEK_SET)
file.truncate()
file.writelines(buff)
file.seek(os.SEEK_SET)

print("---", file.name, "(after capitalization)", "begin", "---")
print(file.read())
print("---", file.name, "end", "---")

file.close()
