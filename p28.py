def linear_search(list, val):
    for x in range(0, len(list)):
        if list[x] == val:
            return x
    return False


def binary_search(list, val):
    l = 0
    r = len(list)-1
    while l <= r:
        m = l + (r-l)//2
        if list[m] == val:
            return m
        elif list[m] < val:
            l = m+1
        else:
            r = m-1
    return False


nums = [50, 23, 9, 18, 61, 32]
val = 50
print(f"Linear Searched val : {linear_search(nums, val)}")
print(f"Binary Searched val : {binary_search(sorted(nums), val)}")
