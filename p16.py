n = 3

sum = 0
for x in range(1, n+1):
    sum = sum + 1/x

print(f"1 + 1/2 + 1/3 + ... + 1/n = {sum}")

sum = 0
for x in range(1, n+1):
    sum = x**(x-1) + sum

print(f"1 + 2^2/2 + 3^3/3 + ... + n^n/n = {sum}")
