class Employee(object):
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return f"{self.name} is an employee."


class Salaried(Employee):
    def __init__(self, name):
        super(Salaried, self).__init__(name)

    def __str__(self):
        return f"{self.name} is a salaryman."


class Waged(Employee):
    def __init__(self, name):
        super(Waged, self).__init__(name)

    def __str__(self):
        return f"{self.name} is daily-waged."


class Commissioned(Employee):
    def __init__(self, name):
        super(Commissioned, self).__init__(name)

    def __str__(self):
        return f"{self.name} eats from commission."


print(Employee("Alex Anderson"))
print(Salaried("Corey Simpson"))
print(Waged("Tony White"))
print(Commissioned("Samantha Willson"))
