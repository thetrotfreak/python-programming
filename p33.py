countries_currencies = {
    "Canada": "Canadian Dollar",
    "China": "Chinese Yuan",
    "Germany": "Euro",
    "India": "Indian Rupee",
    "Japan": "Yen",
}

for country, currency in countries_currencies.items():
    print(country, ":", currency)
