class Time:
    def __init__(self, time: list[int]):
        self.__hour, self.__min, self.__sec = [x for x in time]

    def __add__(self, time: 'Time') -> 'Time':
        hh = self.__hour + time.__hour
        mm = self.__min + time.__min
        ss = self.__sec + time.__sec

        if ss >= 60:
            ss -= 60
            mm += 1

        if mm >= 60:
            mm -= 60
            hh += 1

        if hh > 23:
            hh -= 24

        return Time([hh, mm, ss])

    def __sub__(self, time: 'Time') -> 'Time':
        hh = self.__hour - time.__hour
        mm = self.__min - time.__min
        ss = self.__sec - time.__sec

        if ss < 0:
            ss += 60
            mm -= 1

        if mm < 0:
            mm += 60
            hh -= 1

        if hh < 0:
            hh += 24
        return Time([hh, mm, ss])

    def __str__(self) -> str:
        return f"{self.__hour}h:{self.__min}m:{self.__sec}s"


print("Enter Time as HH:MM:SS")

t1 = input("Time 1? ").split(":")
t1 = [int(x) for x in t1]

t2 = input("Time 2? ").split(":")
t2 = [int(x) for x in t2]


print(f"{Time(t2)} forward from {Time(t1)} is {Time(t1) + Time(t2)}")
print(f"{Time(t2)} backward from {Time(t1)} is {Time(t1) - Time(t2)}")
