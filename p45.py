file1 = open("p43.py", "rt")
file2 = open("p44.py", "rt")

set_file1 = set()
set_file2 = set()

for line in file1:
    set_file1.add(line.rstrip())

for line in file2:
    set_file2.add(line.rstrip())

print("Lines not in", file1.name, ":",
      set_file2.difference(set_file1))
print("Lines not in", file2.name, ":",
      set_file1.difference(set_file2))

file2.close()
file1.close()
