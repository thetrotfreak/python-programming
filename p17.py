n = int(input("Enter nth term for Fibonacci Sequence? "))

fib = [0, 1]
i = 0
while n > len(fib):
    fib.append(fib[i] + fib[i+1])
    i = i + 1

print(fib)
