def is_palindrome(text):
    text = str(text)
    if text[::] == text[::-1]:
        return True
    else:
        return False


text = input("Enter a string? ")
if is_palindrome(text):
    print(f"{text} is a Palindromic string.")
else:
    print(f"{text} is not a Palindromic string.")
