import sys

try:
    p = int(sys.argv[1])
    q = int(sys.argv[2])
    r = p/q

except IndexError:
    print("No arguments were passed")

except ValueError:
    print("Invalid arguments were passed")

except ZeroDivisionError:
    print("Q cannot be 0")

else:
    print(f"{p}/{q} = {r}")
