age = "20"

if not type(age) is int:
    raise TypeError("Needs a valid age!")
else:
    print("Age", age)
