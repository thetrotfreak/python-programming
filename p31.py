def transpose(m):
    t = []
    for i in range(len(m[0])):
        tr = []
        for row in m:
            tr.append(row[i])
        t.append(tr)
    return t


m = [
    [1, 2],
    [3, 4],
    [5, 6]
]


print(f"Matrix : {m}")
print(f"Transpose : {transpose(m)}")
