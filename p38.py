class Distance:
    def __init__(self, foot=0, inch=0):
        self.foot = foot
        self.inch = inch

    def add(self, other):
        inch = self.inch + other.inch
        foot = self.foot + other.foot

        if inch >= 12:
            inch -= 12
            foot += 1

        return Distance(foot, inch)

    def __str__(self):
        return f"{self.foot}ft {self.inch}in"


d1 = Distance(4, 66)
d2 = Distance(7, 10)
print(d1, "+", d2, "=", d1.add(d2))
