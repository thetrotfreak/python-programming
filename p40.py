file = open("p40.py", "rt")
print("Reading from the file", file.name)
for line in file.readlines():
    print("   ", line)
file.close()

file = open("README.txt", "wt")
line = "You must go through a README before jumping to do things!"
file.write(line)
print("Wrote", "'", line, "'", "to", file.name)
file.close()
