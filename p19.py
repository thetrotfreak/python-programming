from math import sqrt


def primality(n):
    if n < 2:
        return False
    for x in range(2, int(sqrt(n)) + 1):
        if n % x == 0:
            return False
    return True


n = int(input("Enter a number? "))

prime_flag = primality(n)

if prime_flag:
    print(f"{n} IS A PRIME.")
else:
    print(f"{n} IS NOT A PRIME.")
