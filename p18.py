n = int(input("Enter a number? "))

sum_odd = 0
sum_even = 0
for x in range(1, n+1):
    if x % 2 != 0:
        sum_odd = sum_odd + x
    else:
        sum_even = sum_even + x

print(f"Sum of ODDs = {sum_odd}")
print(f"Sum of EVENs = {sum_even}")
