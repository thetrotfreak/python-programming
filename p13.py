a = int(input("First Number? "))
b = int(input("Second Number? "))

if a == b:
    print(f"GCD({a},{b}) = {a}")
elif a == 0:
    print(f"GCD({a},{b}) = {b}")
elif b == 0:
    print(f"GCD({a},{b}) = {a}")
else:
    n1 = a
    n2 = b
    r = a % b

    while r > 0:
        a = b
        b = r
        r = a % b

    print(f"GCD({n1},{n2}) = {b}")
