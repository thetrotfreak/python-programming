import os


file = open("p46.txt", "rt+")

print("---", file.name, "begin", "---")
print(file.read())
print("---", file.name, "end", "---")

file.seek(os.SEEK_SET)
buff = file.readlines()

delete_index = int(input("Delete Line# ? "))
print("Deleted", "'", buff.pop(delete_index-1), "'",
      "from", file.name, "at line", delete_index)

file.seek(os.SEEK_SET)
file.truncate()

sentence = input("Line : ")
add_index = int(input("Add at Line# ? "))


buff.insert(add_index-1, sentence+"\n")
print("Wrote", "'", sentence, "'", "to", file.name, "at line", add_index)

file.writelines(buff)
file.close()
