class MyReactangle:
    def __init__(self, l=1, w=1):
        self.__length = l
        self.__width = w

    def area(self):
        return self.__length*self.__width


l = eval(input("Reactangle Length? "))
w = eval(input("Reactangle Width? "))

r1 = MyReactangle(l, w)
print(f"\nReactangle:\nLength = {l} \nWidth = {w} \
\nArea = {r1.area()} sq. units")
