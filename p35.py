set1 = {"curry", "rice", "fish"}
set2 = {"bun", "cake", "chocolate"}

print("Intersection :", set1.intersection(set2))
print("Union :", set1.union(set2))
print("Difference :", set1.difference(set2))
print("Symmetric Difference :", set1.symmetric_difference(set2))
