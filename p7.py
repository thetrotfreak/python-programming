celsius = float(input("Temperature in Celsius = "))
fahrenheit = celsius * 9/5 + 32
print(f"{celsius} \u00B0C = {fahrenheit} \u00B0F")
