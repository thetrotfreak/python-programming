tuple1 = ("a", "b", "c")
tuple2 = (1, 2, 3)

tuple3 = tuple1+tuple2
print("Tuple 1 :", tuple1)
print("Tuple 2 :", tuple2)
print("Merged :", tuple3)
print("Compared :", tuple1 == tuple2)

a, b, c = tuple1
print("Tuple :", tuple1)
print("Splitted :", a, b, c)
